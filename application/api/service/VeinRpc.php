<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 10:14
 */

namespace app\api\service;


use app\common\exception\ConfigException;
use app\common\exception\GrpcException;
use app\common\traits\SingletonTrait;
use Protobuf\AddVeinReq;
use Protobuf\DelVeinReq;
use Protobuf\MatchVeinReq;
use Protobuf\VeinServiceClient;

class VeinRpc
{
    use SingletonTrait;
    const VALIDATE_UP = 0.65;
    const VALIDATE_DOWN = 0.60;

    private $rangeFlag;

    private $grpcClient;

    private function __construct(string $veinRpcHOST)
    {

        if (!$veinRpcHOST) {
            throw new ConfigException('没有配置指静脉远程验证服务器信息');
        }

        $this->grpcClient = new VeinServiceClient($veinRpcHOST, [
            'credentials' => \Grpc\ChannelCredentials::createInsecure(),
        ]);

    }

    public function setRangeFlag(string $flag)
    {
        $this->rangeFlag = $flag;
        return $this;
    }


    public function singleMatch(string $img, string $model)
    {
        $matchVeinReq = new MatchVeinReq();
        $matchVeinReq->setRangeFlag($this->rangeFlag);
        $matchVeinReq->setImg(base64_decode($img));
        $matchVeinReq->setModel(base64_decode($model));
        $matchVeinReq->setMatchType('single');

        $result = $this->grpcClient->MatchVein($matchVeinReq)->wait();

        return $this->parseResult($result);

    }

    public function multipleMatch(string $img)
    {
        $matchVeinReq = new MatchVeinReq();
        $matchVeinReq->setRangeFlag($this->rangeFlag);
        $matchVeinReq->setImg(base64_decode($img));
        $matchVeinReq->setMatchType('multi');
        $result = $this->grpcClient->MatchVein($matchVeinReq)->wait();

        return $this->parseResult($result);

    }


    public function delVeinFeature(string $model)
    {
        $delVeinReq = new DelVeinReq();
        $delVeinReq->setRangeFlag($this->rangeFlag);
        $delVeinReq->setModel($model);

        $result = $this->grpcClient->DeleteVein($delVeinReq)->wait();

        return $this->parseResult($result);

    }

    public function delVeinOptimizeFeature(string $img)
    {

    }


    public function addVeinOptimizeFeature(string $img)
    {

    }

    public function addVeinFeature(array $imgs)
    {

        $ActionIns = new AddVeinReq();
        $ActionIns->setRangeFlag($this->rangeFlag);
        $ActionIns->setImg1(base64_decode($imgs[0]));
        $ActionIns->setImg2(base64_decode($imgs[1]));
        $ActionIns->setImg3(base64_decode($imgs[2]));

        $result = $this->grpcClient->CreateVein($ActionIns)->wait();

        return $this->parseResult($result);
    }

    private function parseResult($data)
    {

        list($reply, $status) = $data;

        if (is_null($reply)) {
            $errNum = 11;
            $srrMsg = $status->details;
        } else {
            $errNum = $reply->getErrNum();
            $srrMsg = $reply->getErrMsg();
            $result = $reply;
        }
        if($errNum){
            throw new GrpcException('Grpc请求异常:'.$srrMsg);
        }
        return $reply;
    }

}
