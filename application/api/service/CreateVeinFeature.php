<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 15:38
 * description:自动更新指静脉特征库(每天一次)
 */

namespace app\api\service;


use app\common\exception\VeinRpcException;
use  app\common\exception\WarningException;
use Common\Model\Device\VeinUserModel;
use  app\api\service\VeinRpc;
use Common\Tools\RedisClient;
use think\cache\driver\Redis;

class CreateVeinFeature {
    private $rule;
    private $redis;
    private $rpcClient;
    private $newFeature;
    private $oldFeature;

    public function __construct(string $flag, string $newFeature) {
        $redis = new Redis();
        $this->redis = $redis->getRedis();
        $this->rpcClient =   veinRpc($flag);
        $this->newFeature = $newFeature;
    }

    /**
     * 更新指静脉特征入口方法
     * @param $userInfo
     * @param $valid_rst
     * @return bool
     * @throws WarningException
     * @throws \Throwable
     */
    public function autoCreateVeinFeature($userInfo, $valid_rst) {

        $this->setAutoCreateVeinConfig();
        $optimizeUser = VeinUserModel::getUserByBindId($userInfo['id']);
        foreach ($this->rule as $key => $rule) {
            //满足要求的特征才会写入更新
            if ($valid_rst['score'] >= $this->rule[$key][0][0] && $valid_rst['score'] <= $this->rule[$key][0][1] && $valid_rst['quality_score'] >= $this->rule[$key][1]) {
                $veinTag = "Vein:Optimize:" . ($key + 7) . ":{$userInfo['id']}";
                //每天更新一次
                if (!$this->redis->get($veinTag)) {
                    $rst_rpc = $this->createSingleVeinFeature();
                    $this->updateUserVeinFeature($rst_rpc, $userInfo, $optimizeUser, $key + 1);
                    $this->redis->set($veinTag, 1, strtotime(date('Y-m-d', strtotime("+1 day"))) - time());
                    break;
                }
            }
        }
    }

    /**
     * 配置指静脉特征更新规则
     */
    private function setAutoCreateVeinConfig() {
        $conf = C('AUTO_CREATE_VEIN_CONF');
        $this->rule[] = explode('|', $conf[0]);
        $this->rule[] = explode('|', $conf[1]);
        $this->rule[0][0] = explode('-', $this->rule[0][0]);
        $this->rule[1][0] = explode('-', $this->rule[1][0]);
    }

    /**
     * 更新用户指静脉特征
     * @param $rst_rpc
     * @param $userInfo
     * @param $optimizeUser
     * @param $level
     * @throws \Throwable
     */
    private function updateUserVeinFeature($rst_rpc, $userInfo, $optimizeUser, $level) {
        try {
            M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))->startTrans();
            $this->addVeinOptimizeFeature($rst_rpc['model']);

            $this->updateDbUserVeinFeature($rst_rpc, $userInfo, $optimizeUser, $level);
            if ($this->oldFeature) {
                //删除旧模板
                $this->delVeinOptimizeFeature($this->oldFeature);
            }
            M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))->commit();
        } catch (\Throwable $e) {
            M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))->rollback();
            $this->delVeinOptimizeFeature($rst_rpc['model']);
            throw $e;
        }
    }

    /**
     * 更新用户Db中指静脉特征
     * @param $rst_rpc
     * @param $user_info
     * @param $optimize_userinfo
     */
    private function updateDbUserVeinFeature($rst_rpc, $userInfo, $optimizeUserinfo, $level) {

        if (!$optimizeUserinfo['extend_id']) {
            $insert['bind_id'] = $userInfo['id'];
            $insert['vein_feature_' . $level] = $rst_rpc['model'];
            $insert['vein_feature_' . $level . '_md5'] = md5($rst_rpc['model']);
            $insert['create_feature_src_' . $level] = $this->newFeature;
            $insert['create_feature_time_' . $level] = date('Y-m-d H:i:s', time());
            $res = M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))->add($insert);
            isSqlError($res, '添加指静脉DB特征库失败');
        } else {

            $this->oldFeature = $optimizeUserinfo['vein_feature_' . $level];

            $res = M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))
                ->where(['extend_id' => $optimizeUserinfo['extend_id']])
                ->save(['vein_feature_' . $level          => $rst_rpc['model'],
                        'vein_feature_' . $level . '_md5' => md5($rst_rpc['model']),
                        'create_feature_src_' . $level    => $this->newFeature,
                        'create_feature_time_' . $level   => date('Y-m-d H:i:s', time())]);
            isSqlError($res, '更新指静脉DB特征库失败');
        }

    }


    /**
     * 创建新的指静脉特征
     * @return bool|mixed|string
     * @throws WarningException
     */
    private function createSingleVeinFeature() {
        $result = $this->rpcClient->createSingleVeinFeature($this->newFeature);
        if (!isset($result['error']) || $result['error'] != 0) {
            throw new VeinRpcException('指静脉RPC请求异常:' . json_encode($result));
        }
        return $result;
    }

    /**
     * 删除用户refis中指静脉特征
     * @throws VeinRpcException
     */
    private function delVeinOptimizeFeature() {
        $result = $this->rpcClient->delVeinOptimizeFeature($this->oldFeature);
        if (!isset($result['error']) || $result['error'] != 0) {
            dingText('指静脉RPC请求异常:响应' . json_encode($result));
        }
    }

    /**
     * 删除用户refis中指静脉特征
     * @throws VeinRpcException
     */
    private function addVeinOptimizeFeature($newFeature) {
        $result = $this->rpcClient->addVeinOptimizeFeature($newFeature);
        if (!isset($result['error']) || $result['error'] != 0) {
            throw new VeinRpcException('指静脉RPC请求异常:' .  json_encode($result));
        }
    }
}
