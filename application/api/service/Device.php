<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/4
 * Time: 17:32
 * description:描述
 */

namespace app\api\service;


use \app\open\model\Device as DeviceModel;
use app\open\model\DeviceType as DeviceTypeModel;
use think\Db;
use think\db\Query;

class Device {

    private $model;

    public function __construct(DeviceModel $model) {
        $this->model = $model;
    }



    public function addDevice(array $data) {
        Db::transaction(function ()use($data){
            $result =Db::table('open_device')->insertGetId($data);
            isModelFailed($result, '添加场馆失败');
            $result=Db::table('open_device_extend')->insert(['did'=>$result]);
            isModelFailed($result, '添加场馆失败');
        });
    }

    public function delDeviceById(int $id, $firmId) {
        $device=$this->model->where('id',$id)->where('firm_id',$firmId)->findOrEmpty()->toArray();
        isEmptyInDb($device, '商户没有指定设备!');

        $result = $this->model->destroy($id);
        isModelFailed($result, '删除设备失败');
        return $result;
    }


}
