<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 16:22
 */

namespace app\api\service\Authentication;


use app\api\service\CreateVeinFeature;
use app\api\service\VeinRpc;


abstract class VeinAuth extends AbstractAuth {


    protected $validateImg;
    protected $validateUser;
    protected $validateResult;

    protected $validateUp;
    protected $validateDown;
    protected $rpcClient;

    public function __construct(array $config) {
        $this->config = $config;
        $this->validateUp = $config['validate_up'] ?: VeinRpc::VALIDATE_UP;
        $this->validateDown = $config['validate'] ?: VeinRpc::VALIDATE_DOWN;
        $this->rpcClient= veinRpc( rangeflag($config['firm_id'],$config['space_id']));
    }



//    /**
//     * 验证后购置
//     * @param int $validateStatus
//     * @throws \app\common\exception\WarningException
//     * @throws \Throwable
//     */
//    protected function _after(int $validateStatus) {
//        try{
//            if ($this->validateResult['quality_score'] >= 0.05 && $validateStatus == 2) {
//                //图片质量和通过验证
//                $VeinService = new CreateVeinFeature($this->config['appid'], $this->validateImg);
//                //按图片质量进行特征自生成
//                $VeinService->autoCreateVeinFeature($this->validateUser, $this->validateResult);
//            }
//        }catch(\Throwable $e){
//            report('更新指静脉特征信息异常:'.$e->getMessage());
//        }
//    }
//
//    protected function isVeinRpcWarning($res,string $message){
//        if (!isset($res['error'])||$res['error']!=0) {
//            report('指静脉RPC请求异常:'.$message.' 响应' . json_encode($res));
//        }
//    }
}
