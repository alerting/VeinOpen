<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/01/16
 * Time: 16:22
 */

namespace app\api\service\Authentication;





 use app\common\exception\AuthException;
 use app\common\exception\DbException;
 use app\common\exception\WarningException;
 use Common\Model\Device\VeinUserModel;
 use Common\Tools\AipFace\AipFace;

 class AipFaceAuth extends AbstractAuth {

     protected $validateResult;

    public function __construct(array $config) {
       $this->config=$config?:C('AipFaceConf');
    }

    public function handle(array $auth){
        //检查参数
        $this->checkInfo($auth);
        $this->authInfo = $auth;
        $result=$this->validate();
        $userType=substr($result['user_id'],0,1);
        $userType = ($userType=='m'?0:($userType=='e'?1:2));
        $userId=substr($result['user_id'],1);
        $validateUser=VeinUserModel::getUserInfoByIdAndUserType($userId,$userType,$this->authInfo['busId']);
        if (!$validateUser) {
            $sql = M()->_sql();
            throw new   DbException("百度人脸识别获取用户信息失败:sql=" . $sql);
        }
        $validateUser['user_type']=$userType;
        return ['user' => $validateUser, 'status' => 1];

    }


     private function checkInfo($info) {
         if (!$info['imgData']) {
             throw new WarningException('缺少参数');
         }
     }

     /**
      * 验证
      */
     private function validate() {
         $aipClient = new AipFace($this->config['APPID'], $this->config['APPTOKEN'], $this->config['SECRETKEY']);
         $result=$aipClient->search($this->authInfo['imgData'], 'BASE64', $this->authInfo['busId']);
         if($result['error_code']!=0){
             throw new AuthException('人脸认证失败:'.$result['error_msg'],$result['error_code']);
         }
        return $result['result']['user_list'][0];
     }


}
