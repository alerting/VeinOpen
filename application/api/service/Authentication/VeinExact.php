<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 16:21
 */

namespace app\api\service\Authentication;


use app\api\model\Member;
use app\common\exception\AuthException;


/**
 * Class veinCommon
 * @package app\api\service\Authentication
 * description:指静脉通用(一对多)认证方式
 */
class VeinCommon extends VeinAuth
{


    public function handle(array $auth)
    {
        $this->authInfo = $auth;
        //遍历验证指静脉信息(生成特征的md5)
        $this->validateVeinModel();

        $this->getUserByModelMd5();
        $validateStatus = $this->checkValidateResult();
//        $this->_after($validateStatus);
        $this->validateUser=array_filter($this->validateUser,function ($key){
            return in_array($key,['vein_uid','user_id','phone','bind_time','space_id','four_phone_num']);
        },ARRAY_FILTER_USE_KEY);
        return ['user' => $this->validateUser, 'status' => $validateStatus, 'info' => $this->validateResult];
    }



    /**
     * 遍历验证指静脉信息(生成特征的md5)
     */
    private function validateVeinModel()
    {
        $img = $this->authInfo['imgData'];
        $rpcResult = $this->rpcClient->multipleMatch($img);
        $this->validateResult['score'] = $rpcResult->getVerifyScore();
//        $this->validateResult['score']=$rpcResult->getVerifyScore() ;
        $this->validateImg = $img;
    }

    /**
     * 通过指静脉特征md5获取用户信息
     * @throws \Exception
     */
    public function getUserByModelMd5()
    {
//        $this->validateResult['vein_model'] = 'Base';
        $this->validateUser = Member::alias("m")
            ->field('m.id,m.vein_uid,m.phone,m.four_phone_num,m.user_id')
            ->join("open_member_extend me", "  on m.id = me.mid", 'left')
            ->where(['m.space_id' => $this->config['space_id'],
                'a.vein_feature_md5' => $this->validateResult['model_md5'],])
            ->find();


        //todo 扩展库验证
        //todo 更新扩展库
//        if (!$this->validateUser) {
//            $this->validateResult['vein_model'] = 'Optimize';
//            //TODO从优化特征码查询 1 2 free
//            $optimizeUserinfo = M("Device_user_extend", C('DB_PREFIX'), C('DB_DEVICE'))->alias("a")
//                ->field('b.id,b.vein_uid,b.phone,b.four_phone_num,b.user_type')
//                ->join("left join rb_device_user b on b.id = a.bind_id")
//                ->where(['b.space_id' => $this->config['space_id'],
//                    '_string' => 'a.vein_feature_1_md5=\'' . $this->validateResult['model_md5'] . '\' or a.vein_feature_2_md5=\'' . $this->validateResult['model_md5'] . '\''])
//                ->select();
//
//            $this->validateUser = $optimizeUserinfo[0];
//        }
//
        if (!$this->validateUser) {
            try {
                $model=$this->validateResult['model'];
                if ($model) {
                    $res = $this->rpcClient->delVeinFeature($model);
                }
            }catch(\Throwable $e){
                report('删除无效Vein模板失败:' . $e->getMessage() . 'modle=' . $model);
                logToFile('删除无效Vein模板失败:' . $e->getMessage() . 'modle=' . $model, 'vein');
            }
            throw new AuthException('认证失败,请重新放入手指!');
        }
    }

    protected function checkValidateResult()
    {
        //判断是或否通过验证
        $validateStatus = self::FAIL;
        if ($this->validateResult['score'] >= $this->validateDown && $this->validateResult['score'] != 1) {
            $validateStatus = self::UNDETERMINED; //待定
        }
        if ($this->validateResult['score'] >= $this->validateUp && $this->validateResult['score'] != 1) {
            $validateStatus = self::SUCCESS; //通过验证
        }
        return $validateStatus;
    }



}
