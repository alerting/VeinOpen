<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 16:20
 */

namespace app\api\service\Authentication;

use app\api\model\Member;



/**
 * Class veinSingle
 * @package app\api\service\Authentication
 * description:指静脉通用(一对一)认证方式
 */
class VeinPhone extends VeinAuth {


    public function handle(array $auth) {
        //检查参数
        $this->authInfo = $auth;
        //查询后四位的sjs用户
        $users = $this->getUsersByFourPhone($auth['fourPhone']);
        //遍历验证指静脉信息
        $this->singleVein($users);
        $validateStatus = $this->checkValidateResult();
//        $this->_after($validateStatus);
        $this->validateUser=array_filter($this->validateUser,function ($key){
            return in_array($key,['vein_uid','user_id','phone','bind_time','space_id']);
        },ARRAY_FILTER_USE_KEY);
        return ['user' => $this->validateUser, 'status' => $validateStatus, 'info' => $this->validateResult];
    }


    /**
     * 根据手机后四位获取用户列表
     * @param $fourPhone
     * @return mixed
     * @throws \Exception
     */
    private function getUsersByFourPhone(int $fourPhone) {
        $user = new Member();
        $users=$user->getMembersDetailByFourPhone($fourPhone,$this->config['space_id']);
        if (!$users) {
            throw new \Exception('手机号码后四位不存在,请重新输入手机号');
        }
        return $users;
    }

    /**
     * 验证
     * @param $users
     */
    public function singleVein($members) {

        $img = $this->authInfo['imgData'];
        $max = 0;
        $veinModel = 'Base';
        $validateUser = [];
        $validateImg='';
        $validateResult=['score'=>0];

        foreach ($members as $member) {
            $feature_array[] = $member['vein_feature'];
            $feature_array[] = $member['vein_feature_1'] ?: '';
            $feature_array[] = $member['vein_feature_2'] ?: '';
            $feature_array=array_filter($feature_array,function ($value){
                return !empty($value);
            });
            foreach ($feature_array as $key => $feature) {

                $singleResult = $this->rpcClient->singleMatch($img, $feature);
                $score=$singleResult->getVerifyScore();
                if ($score > $max && $score != 1) {
                    $max = $score;
                    $validateUser = $member;
                    $validateImg = $img;
                    $validateResult['score'] = $score;
//                    $validateResult['score'] = $score;
                    if ($key > 0) {
                        $veinModel = 'Optimize';
                    }
                }
            }
        }

        unset($validateUser['vein_feature']);
        unset($validateUser['vein_feature_1']);
        unset($validateUser['vein_feature_2']);
        $this->validateImg = $validateImg;
        $this->validateUser = $validateUser;
        $this->validateResult = array_merge($validateResult, ['veinModel' => $veinModel]);
    }

    protected function checkValidateResult() {
        //判断是或否通过验证
        $validateStatus = self::FAIL;
        if ($this->validateResult['score'] >= $this->validateDown && $this->validateResult['score'] != 1) {
            $validateStatus = self::SUCCESS; //待定
        }
        return $validateStatus;
    }

}
