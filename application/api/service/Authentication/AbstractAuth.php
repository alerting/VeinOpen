<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 16:22
 */

namespace app\api\service\Authentication;



abstract class AbstractAuth {
    const SUCCESS='SUCCESS';
    const UNDETERMINED='UNDETERMINED';
    const FAIL='FAIL';

    protected $config;
    protected $authInfo;

    public function __construct(array $config) {
        $this->config = $config;
    }

    abstract public function handle(array $auth);


}
