<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 17:32
 * description:描述
 */

namespace app\api\service;


use \app\api\model\Member as MemberModel;
use \app\api\model\Device as DeviceModel;
use app\api\service\Authentication\VeinAuth;
use app\common\exception\GrpcException;
use http\Exception\RuntimeException;
use think\Db;

class Member
{

    private $model;
    private $device;

    public function __construct(MemberModel $model, DeviceModel $device)
    {
        $this->model = $model;
        $this->device = $device;
    }

    /**
     * 用户绑定指静脉
     * @param array $data
     * @param array $device
     * @param int $firmId
     * @return mixed
     * @throws GrpcException
     * @throws \Throwable
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function bindMember(array $data, array $device, int $firmId)
    {
        $spaceId = $data['spaceId'];
        //这个user_id在商家唯一
        $member = $this->model->where('firm_id', $firmId)->where('user_id', $data['userId'])->findOrEmpty()->toArray();
        if ($member) {
            throw new \RuntimeException('存在重复的user_id');
        }

        //验证手机号在场馆唯一
        $member = $this->model->where('firm_id', $firmId)->where('space_id', $spaceId)
            ->where('phone', $data['phone'])->findOrEmpty()->toArray();
        if ($member) {
            throw new \RuntimeException('该手机号已经在场馆绑定');
        }

        //验证是否已经在特征库
        $rangeFlag = rangeflag($firmId, $spaceId);
        if(key_exists('validate_binding',$device)&&$device['validate_binding']){
            foreach ($data['imgDataArray'] as $img) {
                $rpcDetail = veinRpc($rangeFlag)->multipleMatch($img);
                if ($rpcDetail->getVerifyScore() > $device ? $device['validate'] : 0.65) {
                    throw new \RuntimeException('存在相识指静脉特征,请更换手指');
                }
            }
        }
        //GRPC添加特征库

        $rpcDetail = veinRpc($rangeFlag)->addVeinFeature($data['imgDataArray']);

        $model = $rpcDetail->getModel();
        try {
            $vein_feature = base64_encode($model);
            $member = [
                'firm_id' => $firmId,
                'space_id' => $spaceId,
                'vein_uid' => md5($firmId . $spaceId . $data['userId'] . time()),
                'user_id' => $data['userId'],
                'phone' => $data['phone'] ?: '0',
                'four_phone_num' => $data['phone'] ? substr($data['phone'], -4) : '0',
                'bind_time' => time(),
                'edit_time' => time(),
                'vein_feature' => $vein_feature,
                'vein_feature_md5' => md5($vein_feature),
                'bind_type' => 1,
                'src_img' => json_encode($data['imgDataArray']),
            ];
            //添加数据库
            $DbeRsult = $this->model->insert($member);
            isModelFailed($DbeRsult, '添加成员失败');
        } catch (\Throwable $e) {
            try{
                veinRpc($rangeFlag)->delVeinFeature($model);
            }catch (\Throwable $e){
                report('回滚删除Vein模板失败:' . $e->getMessage() . 'modle=' . $model);
                logToFile('回滚删除Vein模板失败:' . $e->getMessage() . 'modle=' . $model, 'vein');
            }
            throw $e;
        }
        return $member['vein_uid'];
    }

    /**
     * @param array $data
     * @param int $firmId
     * @throws GrpcException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function delMember(array $data, int $firmId)
    {
        //这个vein_uid存在
        $member = $this->model->where('firm_id', $firmId)->where('space_id', $data['spaceId'])
            ->where('vein_uid', $data['veinUid'])->findOrEmpty()->toArray();
        if (!$member) {
            throw new \RuntimeException('不存在的成员veinUid');
        }

        //GRPC删除特征库
        $result = veinRpc(rangeflag($firmId, $member['space_id']))->delVeinFeature(base64_decode($member['vein_feature']));

        //todo  扩展特征库删除

        //删除数据库
        $this->model->where('id', $member['id'])->delete();

    }


}
