<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/11
 * Time: 15:38
 */

namespace app\api\service;


use app\common\exception\WarningException;
use app\api\service\Authentication\AbstractAuth;

class AuthCenter {

    private $handler;


    public static $authStrategy = ['VeinCommon','VeinExact', 'VeinPhone','AipFaceAuth'];

    /**
     * 执行认证
     * @param $auth
     * @return mixed
     */
    public function handle($auth) {
        //请求数据兼容调整

        $result = $this->handler->handle($auth);

        return $result;
    }

    /**
     * 设置认证策略
     * @param string $handler
     * @param array  $device
     * @throws WarningException
     */
    public function setHandler(string $handler, array $config) {
        if (!in_array($handler, self::$authStrategy)) {
            throw new WarningException('不存在的认证方式:' . $handler);
        }

        $handler = 'app\api\service\Authentication\\' . ucfirst($handler);
        if (!class_exists($handler)) {
            throw new WarningException('不存在的认证方式:' . $handler);
        }
        if (!(($this->handler = new $handler($config)) instanceof AbstractAuth)) {
            throw new WarningException('错误的认证方式:' . $handler);
        }
        return $this;
    }

}
