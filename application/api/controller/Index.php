<?php

namespace app\api\controller;

use app\api\service\Device as DeviceServer;
use think\App;
use think\Controller;

/**
 * 控制器
 * Class Index
 * @package app\manager\controller
 */
class Index extends Controller {

    private $service;

    public function __construct(App $app = null, DeviceServer $service) {
        parent::__construct($app);
        $this->service = $service;
    }




    /**
     * 添加场馆
     * @return \think\response\Json
     */
    public function deviceInit() {

        $param = $this->request->only(['deviceId','module','deviceTypeId',], 'post');
        $this->validate($param, 'app\api\validate\InitValidate');

        //添加设备

        $this->service->addDevice(['device_id'=>$param['deviceId'],'module'=>$param['module'],'device_type_id'=>$param['deviceTypeId']]);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功');
    }




}
