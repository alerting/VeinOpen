<?php

namespace app\api\controller;

use app\api\service\Authentication\AbstractAuth;
use app\common\exception\AuthException;
use app\common\service\Md5Auth;
use app\api\service\AuthCenter;
use app\api\service\Member as MemberServer;


/**
 * 平台业务接口控制器
 * Class Device
 * @package app\manager\controller
 */
class Web extends Base
{



    /**
     * 绑定成员(添加)
     * @return \think\response\Json
     */
    public function bindMember(MemberServer $memberServer)
    {

        $param = $this->request->only(['imgDataArray', 'userId', 'phone', 'appId', 'sign', 'spaceId'], 'post');
        $this->validate($param, 'app\api\validate\InterfaceValidate.webbind');

        $firm = $this->_getFirm($param['appId']);

        //验证签名
        Md5Auth::check($param, $param['sign'], $firm['appsecret']);
        unset($param['sign']);

        $device = key_exists('deviceId', $param) ? $this->_getDevice($param['deviceId'], $firm['id']) : [];
        $spaceId = $device ? $this->_getSpaceId($device, $param['spaceId']) : $param['spaceId'];

        $param['spaceId'] = $spaceId;
        //添加设备
        $veinUid = $memberServer->bindMember($param, $device, $firm['id']);
        return $this->jsonReturn(REQUEST_SUCCESS,'绑定成功', ['veinUid' => $veinUid]);
    }

    /**
     * 验证成员
     * @return \think\response\Json
     */
    public function auth()
    {

        $param = $this->request->only(['imgData', 'authType', 'fourPhone', 'deviceId', 'spaceId', 'appId', 'sign'], 'post');
        $this->validate($param, 'app\api\validate\InterfaceValidate.webauth');

        $firm = $this->_getFirm($param['appId']);

        //验证签名
        Md5Auth::check($param, $param['sign'], $firm['appsecret']);
        unset($param['sign']);

        $device = key_exists('deviceId', $param) ? $this->_getDevice($param['deviceId'], $firm['id']) : [];
        $spaceId = $device ? $this->_getSpaceId($device, $param['spaceId']) : $param['spaceId'];
        $param['spaceId'] = $spaceId;

        $authCenter = new AuthCenter();
        $authCenter->setHandler($param['authType'], $device);

        $return = $authCenter->handle($param);


        //todo 插入ES日志
        $this->_addEsLog($param,$return, $device, $firm);
        //todo 返回数据
        if ($return['status'] == AbstractAuth::FAIL) {
            throw new AuthException('认证失败,请重新放入手指');
        }
        if ($return['status'] == AbstractAuth::UNDETERMINED && $param['authType'] != 'VeinExact') {
            throw new AuthException('认证失败,请重新放入手指');
        }
        unset($return['info']);
        //插入验证记录库  es?mysql?
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $return);
    }


    /**
     * 删除成员
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function deleteMember(MemberServer $memberServer)
    {
        $param = $this->request->only(['veinUid', 'spaceId', 'sign', 'appId'], 'post');
        $this->validate($param, 'app\api\validate\InterfaceValidate.webdel');
        $firm = $this->_getFirm($param['appId']);
        //验证签名
        Md5Auth::check($param, $param['sign'], $firm['appsecret']);
        unset($param['sign']);

        $device = key_exists('deviceId', $param) ? $this->_getDevice($param['deviceId'], $firm['id']) : [];
        $spaceId = $device ? $this->_getSpaceId($device, $param['spaceId']) : $param['spaceId'];
        $param['spaceId'] = $spaceId;

        $memberServer->delMember($param, $firm['id']);

        return $this->jsonReturn();
    }

}
