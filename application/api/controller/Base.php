<?php

namespace app\api\controller;

use app\api\service\Authentication\AbstractAuth;
use app\common\exception\AuthException;
use app\common\service\Md5Auth;
use app\api\model\Device as DeviceModel;
use app\api\service\AuthCenter;
use app\api\service\Member as MemberServer;
use think\App;
use think\Controller;
use think\Db;
use think\facade\Log;

/**
 * 成员控制器
 * Class Device
 * @package app\manager\controller
 */
class Base extends Controller
{

    private $device;

    public function __construct(App $app = null, DeviceModel $device)
    {
        parent::__construct($app);
        $this->device = $device;
    }


    protected function _getFirm(string $appId)
    {
        $firm = Db::table('open_firm')->where('appid', $appId)->findOrEmpty();
        isEmptyInDb($firm, '不能识别的场馆');
        return $firm;
    }

    protected function _getDevice(string $deviceId, int $firmId)
    {

        $device = $this->device->getDeviceDetail($deviceId, $firmId);
        isEmptyInDb($device, '指定设备不存在');
        if(empty($device['space_id'])){
            throw new \RuntimeException('设备没有划拨到场馆');
        }
        return $device;
    }

    protected function _getSpaceId(array $device, int $spaceId = null): int
    {
        if ($spaceId && $device['space_id'] != $spaceId) {
            throw new \RuntimeException('设备场馆信息不匹配');
        }
        $spaceId = $device['space_id'];
        return $spaceId;
    }

    protected function _addEsLog($param,$verify,$device,$firm)
    {
        $data=[
            'time'=>date('Y-m-y H:i:s'),
            'action'=>'',
            'authType'=>$param['authType'],
            'firm_id'=>$firm['id'],
            'firm_name'=>$firm['firm_name'],
//            'space_name'=>$device['space_name'],
            'space_id'=>$device['space_id'],
            'deviceId'=>$device['device_id'],
            'deviceName'=>$device['device_name'],
            'result'=>$verify['status'],
            'logId'=>Log::getLog('logId')[0],
            'veinUid'=>key_exists('vein_uid',$verify['user'])?$verify['user']['vein_uid']:"",
            'user'=>$verify['user'],
            'score'=>$verify['info']['score'],
            'mode'=>'base',
            'ext'=>'',
        ];
        pushLogEvent('veinVerify', $data);
    }
}
