<?php

namespace app\api\controller;

use app\api\service\Authentication\AbstractAuth;
use app\common\exception\AuthException;
use app\api\service\AuthCenter;
use app\api\service\Member as MemberServer;


/**
 * 设备业务接口控制器
 * Class Device
 * @package app\manager\controller
 */
class Device extends Base
{



    /**
     * 绑定成员(添加)
     * @return \think\response\Json
     */
    public function bindMember(MemberServer $memberServer)
    {

        $param = $this->request->only(['imgDataArray', 'userId', 'deviceId', 'phone', 'appId',], 'post');
        $this->validate($param, 'app\api\validate\InterfaceValidate.devicebind');

        $firm = $this->_getFirm($param['appId']);


        $device = key_exists('deviceId', $param) ? $this->_getDevice($param['deviceId'], $firm['id']) : [];

        $param['spaceId'] = $device['space_id'];
        //添加设备
        $veinUid = $memberServer->bindMember($param, $device, $firm['id']);
        return $this->jsonReturn(REQUEST_SUCCESS,'绑定成功', ['veinUid' => $veinUid]);
    }

    /**
     * 验证成员
     * @return \think\response\Json
     */
    public function auth()
    {

        $param = $this->request->only(['imgData', 'authType', 'fourPhone', 'deviceId',  'appId',], 'post');
        $this->validate($param, 'app\api\validate\InterfaceValidate.deviceauth');

        $firm = $this->_getFirm($param['appId']);


        $device = key_exists('deviceId', $param) ? $this->_getDevice($param['deviceId'], $firm['id']) : [];

        $param['spaceId'] = $device['space_id'];

        $authCenter = new AuthCenter();
        $authCenter->setHandler($param['authType'], $device);

        $return = $authCenter->handle($param);


        $this->_addEsLog($param,$return, $device, $firm);
        if ($return['status'] == AbstractAuth::FAIL) {
            throw new AuthException('认证失败,请重新放入手指');
        }
        if ($return['status'] == AbstractAuth::UNDETERMINED && $param['authType'] != 'VeinExact') {
            throw new AuthException('认证失败,请重新放入手指');
        }
        unset($return['info']);
        //插入验证记录库  es?mysql?
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $return);
    }






}
