<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/04
 * Time: 17:49
 * description:描述
 */

namespace app\api\model;

use think\Model;
use think\model\concern\SoftDelete;

class DeviceExtend extends Model {


    protected $table = 'open_device_extend';
    protected $pk    = 'id';


    /**
     * 模型初始化
     * 模型初始化方法通常用于注册模型的事件操作。
     */
    protected static function init() {
        //TODO:初始化内容
    }
    public function device()
    {
        return $this->belongsTo('Device');
    }


}
