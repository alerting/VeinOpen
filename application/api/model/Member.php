<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/6
 * Time: 17:49
 * description:描述
 */

namespace app\api\model;

use think\Model;
use think\model\concern\SoftDelete;

class Member extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_at';
    protected $defaultSoftDelete = '0';

    protected $table = 'open_member';
    protected $pk = 'id';


    /**
     * 模型初始化
     * 模型初始化方法通常用于注册模型的事件操作。
     */
    protected static function init()
    {
        //TODO:初始化内容
    }

    public function getMembersDetailByFourPhone(int $fourPhone, int $spaceId)
    {
        return $this->alias('m')->leftJoin('open_member_extend me', 'me.mid=m.id')
            ->where('four_phone_num', $fourPhone)->where('space_id', $spaceId)
//            ->field('m.id,m.firm_id',)
            ->select()->toArray();

    }

}
