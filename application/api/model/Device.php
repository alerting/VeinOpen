<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/04
 * Time: 17:49
 * description:描述
 */

namespace app\api\model;

use think\Model;
use think\model\concern\SoftDelete;

class Device extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_at';
    protected $defaultSoftDelete = '0';

    protected $table = 'open_device';
    protected $pk = 'id';


    /**
     * 模型初始化
     * 模型初始化方法通常用于注册模型的事件操作。
     */
    protected static function init()
    {
        //TODO:初始化内容
    }

    public function extendInfo()
    {
        return $this->hasOne('DeviceExtend');
    }

    public function getDeviceDetail(string $deviceId, int $firmId)
    {
        $device = $this->alias('d')->join('open_device_extend de', 'd.id=de.did','left')
            ->where('d.device_id', $deviceId)->where('d.firm_id', $firmId)->findOrEmpty()->toArray();


        $extAttr = key_exists('ext_attr',$device)?json_decode($device['ext_attr'], true):[];

        return array_merge($device,$extAttr);

    }


}
