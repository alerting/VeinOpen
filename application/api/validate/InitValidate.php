<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 16:55
 * description:描述
 */

namespace app\api\validate;


use think\Db;
use think\Validate;

class InitValidate extends Validate {

    protected $rule = ['deviceId' => 'require|chsDash|unique:open_device,device_id',
        'module'=>'require|in:0,1,2',
        'deviceTypeId'=>'require|integer|checkDeviceType:thinkphp'];

    protected $message = ['deviceId.require' => '设备ID不能为空!',
        'deviceId.chsDash' => '设备ID格式错误!',
        'deviceId.unique'  => '设备ID已存在!',
        'module.require' => '模块信息不能为空!',
        'module.in'       => '模块信息错误！',
        'deviceTypeId.integer' => '非法的设备类型!',
        'deviceTypeId.require' => '设备类型不能为空!',
    ];

    // 自定义验证规则
    protected function checkDeviceType($deviceType, $rule, $data = []) {
        $message=null;
        $result = Db::name('open_device_type')->where('id', $deviceType)->findOrEmpty();
        if (!$result) {
            $message='设备信息错误';
        }
        return $message ?:true ;
    }

}
