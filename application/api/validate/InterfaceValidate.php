<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 16:55
 * description:描述
 */

namespace app\api\validate;


use app\api\service\AuthCenter;
use think\Db;
use think\Validate;

class InterfaceValidate extends Validate
{

    protected $rule = ['imgDataArray' => 'checkImgs:thinkphp',
        'authType' => 'checkAuthType:thinkphp',
        'imgData' => 'checkImg:thinkphp',
        'deviceId' => 'alphaDash',
        'userId' => 'alphaDash',
        'veinUid' => 'alphaDash',
        'phone' => 'mobile',
        'appId' => 'require|alphaDash',
        'sign' => 'require|alphaDash',
        'spaceId' => 'requireIf:deviceId,|integer',
        'fourPhone' => 'requireIf:authType,veinPhone|integer',
    ];

    protected $message = ['imgDataArray.require' => '指静脉特征不能为空!',
        'deviceId.alphaDash' => '设备ID格式错误!',
        'phone.mobile' => '电话号码格式错误!',
        'userId.require' => '成员ID不能为空！',
        'userId.alphaDash' => '成员ID格式错误!',
        'sign.require' => '签名不能为空!',
        'sign.alphaDash' => '签名格式错误!',
        'appId.require' => 'appId不能为空!',
        'appId.alphaDash' => 'AppId格式错误!',
        'spaceId.requireIf' => '场馆ID不能为空!',
        'spaceId.integer' => '场馆ID格式错误!',
        'fourPhone.requireIf' => '手机后四位不能为空!',
        'fourPhone.integer' => '手机后四位错误!',

    ];

    // 自定义验证规则
    protected function checkImgs($imgData, $rule, $data = [])
    {
        $message = null;
            if (!is_array($imgData) && count($$imgData) < 3) {
            $message = '指静脉图片信息错误';
        }else{
            foreach ($imgData as $img) {
                if (!$this->checkBase64($img)) {
                    $message = '不能识别的图片';
                    break;
                }
            }
        }
        return $message ?: true;
    }

    // 自定义验证规则
    protected function checkAuthType($fromType, $rule, $data = [])
    {
        $message = null;
        if (!in_array($fromType, AuthCenter::$authStrategy)) {
            $message = '不支持的认证方式';
        }
        return $message ?: true;
    }
    protected function checkImg($imgData, $rule, $data)
    {
        $message = null;
        if (!$this->checkBase64($imgData)) {
            $message = '不能识别的图片';
        }
        return $message ?: true;
    }
    protected function checkBase64($base64)
    {
        $result=($base64 == base64_encode(base64_decode($base64))) ? true : false;
        return $result;
    }

    public function sceneDeviceBind()
    {
        return $this->only(['imgDataArray', 'deviceId', 'userId', 'phone', 'appId',])
            ->append('deviceId', 'require')
            ->append('userId', 'require')
            ->append('phone', 'require');
    }

    public function sceneDeviceAuth()
    {
        return $this->only(['imgData', 'deviceId', 'appId', 'authType','fourPhone'])
            ->append('deviceId', 'require')
            ->append('imgData', 'require')
            ->append('fourPhone', 'require')
            ->append('authType', 'require');
    }

    public function sceneWebDel()
    {
        return $this->only(['veinUid', 'deviceId', 'appId', 'sign', 'spaceId',])
            ->append('veinUid', 'require');
    }
    public function sceneWebBind()
    {
        return $this->only(['imgDataArray', 'deviceId', 'userId', 'phone', 'appId', 'sign', 'spaceId',])
            ->append('userId', 'require')
            ->append('phone', 'require');
    }

    public function sceneWebAuth()
    {
        return $this->only(['imgData', 'deviceId', 'appId', 'sign', 'spaceId', 'authType','fourPhone'])
            ->append('imgData', 'require')
            ->append('fourPhone', 'require')
            ->append('authType', 'require');
    }


}
