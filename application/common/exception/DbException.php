<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/4
 * Time: 17:46
 * description:描述
 */

namespace app\common\exception;


class DbException extends \think\exception\DbException
{
    protected $code = DB_EXCEPTION;
}
