<?php

namespace app\common\service;


use app\common\exception\AuthException;
use think\Db;

/**
 * 系统权限节点读取器
 * Class NodeService
 * @package extend
 * @author Anyon <zoujingli@qq.com>
 * @date 2017/05/08 11:28
 */
class Md5Auth
{

    /**
     * 应用用户权限节点
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function check(array $param, string $sign,string $appSecret)
    {
        $paramStr='';
        ksort($param);
        foreach ($param as $k=>$v){
            if(!is_array($v)&&$v&&$k!=='sign'){
                $paramStr.=$k.'='.$v.'&';
            }
        }
        $signBuild = md5($paramStr . $appSecret);
        if($signBuild !== $sign) {
            throw new AuthException('验签失败!');
        }
    }


}
