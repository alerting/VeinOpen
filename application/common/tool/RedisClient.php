<?php

namespace app\common\tool;


use app\common\exception\ConfigException;
use app\common\traits\SingletonTrait;

/**
 * This is a Redis exntend class
 *
 */
class redisClient
{
    use SingletonTrait;

    private static $linkHandle;

    protected $conf ;

    private function __construct(array $config = [])
    {
        if ( !extension_loaded('redis') ) {
            throw new ConfigException('服务器环境没有redis扩展');
        }
        $this->conf = $config;
    }


    /**
     * 获得redis Resources
     *
     * @param $key   redis存的key/或随机值
     * @param string $tag   master/slave
     */
    public function getRedis($key=null, $tag='master')
    {
        if(!empty(self::$linkHandle[$tag])){
            return self::$linkHandle[$tag];
        }

        empty($key)?$key = uniqid():'';
        $redis_arr  = $this->conf[$tag];
        $arr_index  = $this->getHostByHash($key,count($this->conf[$tag])); //获得相应主机的数组下标

        $obj = new \Redis;
        $obj->pconnect($redis_arr[$arr_index]['host'], $redis_arr[$arr_index]['port']);
        /* 更新redis的密码验证 */
        if ($redis_arr[$arr_index]['auth']) {
            $obj->auth($redis_arr[$arr_index]['auth']);
        }
        self::$linkHandle[$tag] = $obj;
        return $obj;
    }

    /**
     * 随机取出主机
     * @param $key      $变量key值
     * @param $n        主机数
     * @return string
     */
    private function getHostByHash($key, $n)
    {
        if($n<2) return 0;
        $u = strtolower($key);
        $id = sprintf("%u", crc32($key));

        $m = base_convert( intval(fmod($id, $n)), 10, $n);
        return $m{0};
    }

    /**
     * 关闭连接
     * pconnect 连接是无法关闭的
     *
     * @param int $flag 关闭选择 0:关闭 Master 1:关闭 Slave 2:关闭所有
     * @return boolean
     */
    public function close($flag=2)
    {
        switch($flag){
            // 关闭 Master
            case 0:
                foreach (self::$linkHandle['master'] as $var){
                    $var->close();
                }
                break;
            // 关闭 Slave
            case 1:
                foreach (self::$linkHandle['slave'] as $var){
                    $var->close();
                }
                break;
            // 关闭所有
            case 2:
                $this->close(0);
                $this->close(1);
                break;
        }
        return true;
    }

    public function acquire_lock($lock_name, $acquire_time = 3, $lock_timeout = 10)
    {

        $identifier   = md5($_SERVER['REQUEST_TIME'] . mt_rand(1, 10000000));
        $lock_name    = 'lock:' . $lock_name;
        $lock_timeout = intval(ceil($lock_timeout));
        $end_time     = time() + $acquire_time;
        while (time() < $end_time) {
            $script = <<<luascript
                 local result = redis.call('setnx',KEYS[1],ARGV[1]);
                    if result == 1 then
                        redis.call('expire',KEYS[1],ARGV[2])
                        return 1
                    elseif redis.call('ttl',KEYS[1]) == -1 then
                       redis.call('expire',KEYS[1],ARGV[2])
                       return 0
                    end
                    return 0
luascript;
            $result = $this->getRedis()->eval($script, array($lock_name, $identifier, $lock_timeout), 1);
            if ($result == '1') {
                return $identifier;
            }
            usleep(100000);
        }
        return false;
    }


    public function release_lock($lock_name, $identifier)
    {
        $lock_name = 'lock:' . $lock_name;
        while (true) {
            $script = <<<luascript
                local result = redis.call('get',KEYS[1]);
                if result == ARGV[1] then
                    if redis.call('del',KEYS[1]) == 1 then
                        return 1;
                    end
                end
                return 0
luascript;
            $result = $this->getRedis()->eval($script, array($lock_name, $identifier), 1);
            if ($result == 1) {
                return true;
            }
            break;
        }
        //进程已经失去了锁
        return false;
    }


}
