<?php
const REQUEST_SUCCESS = 0; //操作成功
const UNKOWN_EXCEPTION = 90000; //未知异常

const AUTH_EXCEPTION = 10000; //权限异常
const LOGIN_ERROR = 10001; //登陆失败
const HAS_LOGINED_ERROR = 10002; //你已经登陆
const NOT_LOGIN = 10003; //没有登陆
const ACCESS_NODE_FAIL = 10004; //没有访问节点权限

const CONFIG_EXCEPTION = 20000; //配置异常

const EVENT_EXCEPTION = 30000; //事件异常

const REMOTE_EXCEPTION = 40000; //外部请求异常
const GRPC_EXCEPTION = 40001; //外部请求异常

const PARAM_EXCEPTION = 50000; //参数异常

const WARNING_EXCEPTION = 60000; //参数异常

const DB_EXCEPTION = 70000; //参数异常
const DATANOTFOUND_EXCEPTION = 70001; //参数异常
