<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/4
 * Time: 17:32
 * description:描述
 */

namespace app\open\service;


use \app\open\model\Device as DeviceModel;
use app\open\model\DeviceType as DeviceTypeModel;
use think\db\Query;

class Device {

    private $model;

    public function __construct(DeviceModel $model) {
        $this->model = $model;
    }

    public function searchDevices(array $search) {
        $query = new Query();

        foreach (['firm_id','space_id','device_type_id','channel_id','device_status','software_version','fireward_version'] as $key) {
            if ((isset($search[$key]) && $search[$key] !== '')) {
                $query->where('d.'.$key, "$search[$key]");
            }
        }
        foreach (['space_name','channel_name'] as $key=>$value){
            if ((isset($search[$key]) && $search[$key] !== '')) {
                $query->where($key, "$search[$key]");
            }
        }
        if (isset($search['device_id']) && $search['device_id'] !== '') {
            $query->whereLike('d.device_id', "{$search['device_id']}%");
        }

        $result = $this->model->alias('d')->join('open_space os','d.space_id=os.id')
                                ->where($query)->field('d.id,d.firm_id,os.space_name,d.device_name,d.channel_id,d.is_assigned,d.device_status')->paginate(10);
        return $result;
    }

    public function getDeviceById(int $id) {
        $result = $this->model->alias('d')->where(['d.id' => $id,])->join('open_device_extend ex','d.id=ex.did')
                              ->findOrEmpty()->toArray();
        isEmptyInDb($result, '该设备不存在');
        return $result;
    }
    public function getDeviceInFirmById(int $id,int $firmId) {
        $result = $this->model->alias('d')->where(['d.id' => $id,'firm_id'=>$firmId])->join('open_device_extend ex','d.id=ex.did')
                              ->findOrEmpty()->toArray();
        isEmptyInDb($result, '商户没有该设备');
        return $result;
    }

    public function updateDeviceById(int $id, array $data) {

        //分享处理
        $result = $this->model->save($data, ['id' => $id]);
        isModelFailed($result, '修改设备信息失败!');
        return $this->model;
    }
    public function updateDeviceInFirmById(int $id, $firmId, array $data) {

        //分享处理
        $result = $this->model->save($data, ['id' => $id,'firm_id'=>$firmId]);
        isModelFailed($result, '修改设备信息失败!');
        return $this->model;
    }


    public function addDevice(array $data) {
        $result = $this->model->save($data);
        isModelFailed($result, '添加场馆失败');
        return $this->model;
    }

    public function delDeviceById(int $id) {
        $device=$this->model->where('id',$id)->findOrEmpty()->toArray();
        isEmptyInDb($device, '设备不存在!');

        $result = $this->model->destroy($id);
        isModelFailed($result, '删除设备失败');
        return $result;
    }
    public function delDeviceInFirmById(int $id, int $firmId) {
        $device=$this->model->where('id',$id)->where('firm_id',$firmId)->findOrEmpty()->toArray();
        isEmptyInDb($device, '商户没有指定设备!');

        $result = $this->model->destroy($id);
        isModelFailed($result, '删除设备失败');
        return $result;
    }


}
