<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/4
 * Time: 17:32
 * description:描述
 */

namespace app\open\service;

use \app\open\model\Api as ApiModel;
use think\db\Query;

class Api {

    public static $apiTitles=['register','bind','sign','gate','cabinet','comsume'];
    public static $apiNames=['register'=>'用户注册','bind'=>'绑定指静脉','sign'=>'签到','gate'=>'闸机验证','cabinet'=>'中控验证','comsume'=>'消费下单'];
    private $model;

    public function __construct(ApiModel $model) {
        $this->model = $model;
    }

    public function searchApis(int $firmId) {

        $result = $this->model->where('firm_id',$firmId)->field('title,api_name,url,status')->findOrEmpty();
        return $result;
    }



    public function updateApiByFirmId(int $firmId, array $data) {

        //分享处理
        foreach ($data as $key=>$value){
            $api = $this->model->where('firm_id',$firmId)
                        ->where('title',$key)
                        ->findOrEmpty()->toArray();
            if(empty($api)){
                $result=$this->model->save(array_merge($value,['firm_id'=>$firmId,'title'=>$key,'api_name'=>self::$apiNames[$key]]));
            }   else{
                $result=$this->model->save($value, ['firm_id' => $firmId,'title'=>$key]);
            }
            isModelFailed($result, '修改设备信息失败!');

        }

    }





}
