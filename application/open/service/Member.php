<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/4
 * Time: 17:32
 * description:描述
 */

namespace app\open\service;

use \app\open\model\Member as MemberModel;
use think\db\Query;

class Member {

    private $model;

    public function __construct(MemberModel $model) {
        $this->model = $model;
    }

    public function searchMembers(array $search) {
        $query = new Query();

        foreach (['space_id', 'phone','user_id'] as $key) {
            if ((isset($search[$key]) && $search[$key] !== '')) {
                $query->where('m.'.$key, "$search[$key]");
            }
        }
        if (isset($search['space_name']) && $search['space_name'] !== '') {
            $query->whereLike('os.space_name', "{$search['space_name']}%");
        }

        $result = $this->model->alias('m')->join('open_space os','m.space_id=os.id')
                                ->where($query)->field('m.id,os.space_name,m.phone,m.user_id,m.vein_uid,m.bind_time')->paginate(10);
        return $result;
    }

    public function getMemberById(int $id, $firmId) {
        $result = $this->model->where(['id' => $id,'firm_id'=>$firmId])->field('id,space_id,phone,user_id')
                              ->findOrEmpty()->toArray();
        isEmptyInDb($result, '商户没有该成员');
        return $result;
    }

    public function updateMemberById(int $id,int $firmId, array $data) {
        $space=$this->model->where('id',$id)->where('firm_id',$firmId)->findOrEmpty()->toArray();
        isEmptyInDb($space, '商户没有指定成员!');

        $result = $this->model->save($data, ['id' => $id]);
        isModelFailed($result, '修改成员信息失败!');
        return $this->model;
    }

    public function addMember(array $data) {

        $result = $this->model->save($data);
        isModelFailed($result, '添加成员失败');
        return $this->model;
    }

    public function delMemberById(int $id, $firmId) {
        $space=$this->model->where('id',$id)->where('firm_id',$firmId)->findOrEmpty()->toArray();
        isEmptyInDb($space, '商户没有指定成员!');

        $result = $this->model->destroy($id);
        isModelFailed($result, '删除成员失败');
        return $result;
    }



}
