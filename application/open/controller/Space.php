<?php

namespace app\open\controller;

use app\open\service\Space as SpaceServer;
use think\App;
use app\open\controller\OpenBase as Controller;

/**
 * 场馆控制器
 * Class Space
 * @package app\open\controller
 */
class Space extends Controller {

    private $service;


    public function __construct(App $app = null, SpaceServer $service) {
        parent::__construct($app);
        $this->service = $service;
    }

    /**
     * 场馆列表
     * @return \think\response\Json
     */
    public function index() {
        $search = $this->request->only(['space_name','status'],'get');
        $this->validate($search,'app\open\validate\SpaceValidate');
        $search['firm_id']=$this->firmId;

        $result = $this->service->searchSpaces($search);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $result);
    }


    /**
     * 场馆编辑
     * @param $id
     * @return \think\response\Json
     */
    public function edit($id) {
        $user=$this->service->getSpaceById($id,$this->firmId);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $user);
    }

    /**
     * 修改场馆信息
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        //验证数据
        $param = $this->request->only(['space_name','phone','desc','status'],'param');
        $this->validate($param, 'app\open\validate\SpaceValidate');
        //执行更新

        unset($param['id']);
        $this->service->updateSpaceById($id,$this->firmId, $param);
        //返回数据
        return $this->jsonReturn();
    }

    /**
     * 添加场馆
     * @return \think\response\Json
     */
    public function save() {
        $param = $this->request->only(['space_name','desc'],'post');
        $this->validate($param, 'app\open\validate\SpaceValidate.create');

        $param['firm_id']=$this->firmId;
        $param['status']=0;
        $param['create_by']=session('user.id');
        $user=$this->service->addSpace($param);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $user);
    }


    /**
     * 删除场馆
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete($id) {
        $this->service->delSpaceById($id,$this->firmId);
        return $this->jsonReturn();
    }
}
