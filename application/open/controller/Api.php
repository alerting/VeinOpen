<?php

namespace app\open\controller;

use app\open\service\Api as ApiServer;
use think\App;
use app\open\controller\OpenBase as Controller;
use think\exception\ValidateException;

/**
 * API控制器
 * Class Api
 * @package app\open\controller
 */
class Api extends Controller {

    private $service;

    public function __construct(App $app = null, ApiServer $service) {
        parent::__construct($app);
        $this->service = $service;
    }

    /**
     * API列表
     * @return \think\response\Json
     */
    public function index() {

        $result = $this->service->searchApis($this->firmId);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $result);
    }

    /**
     * 修改API信息
     * @param $id
     * @return \think\response\Json
     */
    public function update() {

        $param = $this->request->only(ApiServer::$apiTitles, 'post');
        $data=[];
        foreach ($param as $key=>$value){

            $value=explode('_',$value,2);
            if(!in_array($value[0],['0','1'],true)){
                throw new ValidateException('接口状态非法!');
            }
            if($value[0]&&$value[1]&&!preg_match('/^https?:\\/\\/.+/',$value[1])){
                throw new ValidateException(ApiServer::$apiNames[$key].' 接口地址非法!');
            }
            $data[$key]=[];
            $data[$key]['status']=$value[0];
            $data[$key]['url']=$value[1];
            $data[$key]['edit_by']=session('user.id');
        }

        //执行更新
        $this->service->updateApiByFirmId($this->firmId, $data);
        //返回数据
        return $this->jsonReturn();
    }




}
