<?php

namespace app\open\controller;

use app\open\service\Member as MemberServer;
use think\App;
use app\open\controller\OpenBase as Controller;


/**
 * 成员控制器
 * Class Api
 * @package app\open\controller
 */
class Member extends Controller {

    private $service;

    public function __construct(App $app = null, MemberServer $service) {
        parent::__construct($app);
        $this->service = $service;
    }

    /**
     * 成员列表
     * @return \think\response\Json
     */
    public function index() {
        $search = $this->request->only(['space_id','phone','user_id'],'get');
        $search['firm_id']=$this->firmId;

        $result = $this->service->searchMembers($search);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $result);
    }


    /**
     * 成员编辑
     * @param $id
     * @return \think\response\Json
     */
    public function edit($id) {
        $user=$this->service->getMemberById($id,$this->firmId);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $user);
    }

    /**
     * 修改成员信息
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {
        //验证数据
        $param = $this->request->only(['id','member_name','phone','desc','status'],'param');
        $this->validate($param, 'app\open\validate\MemberValidate');
        //执行更新

        unset($param['id']);
        $this->service->updateMemberById($id,$this->firmId, $param);
        //返回数据
        return $this->jsonReturn();
    }

    /**
     * 添加成员
     * @return \think\response\Json
     */
    public function save() {
        $param = $this->request->only(['member_name','desc'],'post');
        $this->validate($param, 'app\open\validate\MemberValidate');

        $param['firm_id']=$this->firmId;
        $param['status']=0;
        $param['create_by']=session('user.id');
        $user=$this->service->addMember($param);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $user);
    }


    /**
     * 删除成员
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete($id) {
        $this->service->delMemberById($id,$this->firmId);
        return $this->jsonReturn();
    }




}
