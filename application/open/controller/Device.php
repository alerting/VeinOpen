<?php

namespace app\open\controller;

use app\open\service\Device as DeviceServer;
use think\App;
use app\open\controller\OpenBase as Controller;


/**
 * 设备控制器
 * Class Device
 * @package app\open\controller
 */
class Device extends Controller {

    private $service;

    public function __construct(App $app = null, DeviceServer $service) {
        parent::__construct($app);
        $this->service = $service;
    }

    /**
     * 设备列表
     * @return \think\response\Json
     */
    public function index() {
        $search = $this->request->only(['device_id', 'space_name', 'device_type_id', 'channel_name',
                                        'device_status'], 'get');
        $search['firm_id'] = $this->firmId;
        $result = $this->service->searchDevices($search);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $result);
    }


    /**
     * 设备编辑
     * @param $id
     * @return \think\response\Json
     */
    public function edit($id) {

        $device=$this->_getDevice($id,$this->firmId);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $device);
    }
    private function _getDevice(int $id,int $firmId){
        if($firmId===0){
            $device = $this->service->getDeviceById($id, $firmId);
        }else{
            $device = $this->service->getDeviceInFirmById($id, $firmId);
        }
        return $device;
    }
    /**
     * 修改设备信息
     * @param $id
     * @return \think\response\Json
     */
    public function update($id) {

        $param = $this->_checkRequestDataByDeviceType($id, $this->firmId);

        //执行更新
        $this->service->updateDeviceInFirmById($id, $this->firmId, $param);
        //返回数据
        return $this->jsonReturn();
    }

    private function _checkRequestDataByDeviceType(int $id, int $firmId) {
        //验证数据
        $deviceTypeInfo = ['gate' => [1], 'guard' => [2], 'sign' => [3]];

        $device = $this->_getDevice($id,$firmId);

        $param = $this->request->only(['device_name', 'space_id', 'channel_id', 'device_status', 'validate',
                                       'theme',], 'param');
        switch (true) {
            case in_array($device['device_type_id'], $deviceTypeInfo['gate']):
                $extend = $this->request->only(['guard_switch', 'nfc_firm', 'reading_order',
                                                'prompt_message'], 'param');
                $param = array_merge($param, $extend);
                $this->validate($param, 'app\open\validate\DeviceValidate.gateedit');
            case in_array($device['device_type_id'], $deviceTypeInfo['guard']):
                $extend = $this->request->only(['nfc_firm', 'reading_order', 'auth_mode', 'validate_up'], 'param');
                $param = array_merge($param, $extend);
                $this->validate($param, 'app\open\validate\DeviceValidate.guardedit');

            case in_array($device['device_type_id'], $deviceTypeInfo['sign']):
                $extend = $this->request->only(['nfc_firm', 'reading_order', 'auth_mode', 'validate_up', 'validate_binding'], 'param');
                $param = array_merge($param, $extend);
                $this->validate($param, 'app\open\validate\DeviceValidate.signedit');

        }

        return $param;
    }


    /**
     * 添加设备
     * @return \think\response\Json
     */
    public function save() {
        $param = $this->request->only(['space_name', 'desc'], 'post');
        $this->validate($param, 'app\manager\validate\DeviceValidate');

        $param['firm_id'] = $this->firmId;
        $param['status'] = 0;
        $param['create_by'] = session('user.id');
        $user = $this->service->addDevice($param);
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $user);
    }

    /**
     * 删除设备
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete($id) {

        $this->service->delDeviceInFirmById($id, $this->firmId);
        return $this->jsonReturn();
    }
}
