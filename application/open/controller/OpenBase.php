<?php

namespace app\open\controller;

use app\common\exception\AuthException;
use think\Controller;

/**
 * 管理员控制器
 * Class Api
 * @package app\manager\controller
 */
class OpenBase extends Controller {

    protected $firmId;
    /**
     * 控制器基础方法
     */
    protected function initialize()
    {
        $this->firmId=session('user.firm.id');
        if (!$this->firmId) {
            throw new AuthException('账号没有指定商家!');
        }
    }
}
