<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 16:55
 * description:描述
 */

namespace app\open\validate;

use app\open\service\Api;
use think\Db;
use think\Validate;

class ApiValidate extends Validate {

    protected $rule = ['test' => 'chsDash',];

    protected $message = ['test.chsDash' => '!',];


    public function sceneEdit() {
        $validate = $this->only(['test']);
        foreach (Api::$apiTitles as  $value) {
            $validate = $validate->append($value, 'url');
        }
        return $validate;
    }


}
