<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 16:55
 * description:描述
 */

namespace app\open\validate;

use app\common\traits\ValidateTrait;
use think\Db;
use think\Validate;

class DeviceValidate extends Validate {

    protected $rule = ['device_id' => 'require|chsDash|unique:open_device',
                       'device_name'=>'require|chsDash|length:0,32',
                       'space_id' => 'integer', 'device_status' => 'require|in:0,1',
                       'channel_id'=>'integer',
                       'validate'=>'require|float','validate_up'=>'float',
                       'guard_switch'=>'in:0,1','prompt_message'=>'chsDash|length:1,100',
                       'reading_order'=>'in:0,1',   'nfc_firm'=>'chsDash|length:0,32',
                       'validate_binding'=>'in:0,1','auth_mode'=>'in:0,1,2',
                       'theme'=>'in:0,1,2'];

    protected $message = ['device_id.requireCallback' => '设备ID不能为空!',
                          'device_id.chsDash' => '设备ID格式错误!',
                          'device_id.unique'  => '设备ID已存在!',
                          'space_id.integer' => '非法的场馆信息!',
                          'device_status.in'       => '非法的设备状态！',
                          'channel_id.integer' => '非法的设备渠道!',
                          'validate.require' => '认证阀值不能为空!',
                          'validate.float' => '认证阀值格式错误!',
                          'validate_up.float' => '认证上限阀值格式错误!',
                          'guard_switch.in' => '守护鸡状态错误!',
                          'prompt_message.chsDash' => '闸机欢迎语非法!',
                          'prompt_message.length' => '闸机欢迎语长度异常!',
                          'nfc_firm.chsDash' => 'NFC厂商信息错误!',
                          'nfc_firm.length' => 'NFC厂商信息长度异常!',
                          'reading_order.in' => 'nfc读取顺序错误!',
                          'validate_binding.in' => '特征验证参数异常!',
                          'auth_mode.in' => '指静脉认证模式错误!',
                          'theme.in' => '设备主题错误!',
        ];

    public function sceneCreat()
    {
        return $this->only(['device_id','space_id',])
                    ->append('device_id', 'require');

    }
    // edit 验证场景定义
    public function sceneGateEdit()
    {
        return $this->only(['device_name','space_id','channel_id','device_status','validate','theme','guard_switch','nfc_firm','reading_order','prompt_message'])
                    ->append('guard_switch', 'require')
                    ->append('nfc_firm', 'require')
                    ->append('prompt_message', 'require');

    }
    public function sceneSignEdit()
    {
        return $this->only(['device_name','space_id','channel_id','device_status','validate','theme','auth_mode','nfc_firm','reading_order','validate_binding','validate_up'])
                    ->append('validate_up', 'require')
                    ->append('nfc_firm', 'require')
                    ->append('auth_mode', 'require')
                    ->append('validate_binding', 'require');

    }
    public function sceneGuardEdit()
    {
        return $this->only(['device_name','space_id','channel_id','device_status','validate','theme','validate_up','nfc_firm','reading_order','auth_mode'])
                    ->append('validate_up', 'require')
                    ->append('nfc_firm', 'require')
                    ->append('auth_mode', 'require');

    }

}
