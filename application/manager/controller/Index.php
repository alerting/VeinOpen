<?php


namespace app\manager\controller;

use app\common\exception\AuthException;

use app\common\service\AuthService;
use app\common\service\LogService;
use app\manager\service\User as UserServer;
use app\manager\service\Menu as MenuServer;
use think\App;
use think\Controller;
use think\Db;


class Index extends Controller {

    public function __construct(App $app = null) {
        parent::__construct($app);
    }

    /**
     * @param MenuServer $menuServer
     * @return \think\response\Json
     * @throws AuthException
     */
    public function menus(MenuServer $menuServer) {
        $user=session('user');
        if(!key_exists('node',$user)||empty($nodes=$user['nodes'])){
            AuthService::applyAuthNode($user);
            $nodes=session('user.nodes');
        }

        $nodes=array_merge($nodes,['']);
        $menus = $menuServer->getUserMenuTree($nodes,!!$user);
        if (empty($menus) && !session('user.id')) {
            throw new AuthException('没有任何权限');
        }
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $menus);
    }

    /**
     * @param MenuServer $menuServer
     * @return \think\response\Json
     * @throws AuthException
     */
    public function routes(MenuServer $menuServer) {
        $user=session('user');
        if(!key_exists('node',$user)||empty($nodes=$user['nodes'])){
            AuthService::applyAuthNode($user);
            $nodes=session('user.nodes');
        }

        $nodes=array_merge($nodes,['']);
        $menus = $menuServer->getUserRoutes($nodes,!!$user);
        if (empty($menus) && !session('user.id')) {
            throw new AuthException('没有任何权限');
        }
        $menus=array_column($menus,'furl');
        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $menus);
    }


    /**
     * @param UserServer $userServer
     * @return \think\response\Json
     * @throws AuthException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function pass(UserServer $userServer) {
        $data = $this->request->post();
        $userId = session('user.id');
        if ($data['password'] !== $data['repassword']) {
            throw new AuthException('两次输入的密码不一致，请重新输入！');
        }
        $user = Db::name('SystemUser')->where('id', $userId)->find();
        if (md5($data['oldpassword']) !== $user['password']) {
            throw new AuthException('旧密码错误,请重新输入');
        }
        $userServer->updateUserById($userId, [ 'password' => md5($data['password'])]);

        return $this->jsonReturn(REQUEST_SUCCESS, '密码修改成功，下次请使用新密码登录！');
    }

    /**
     * @param UserServer $userServer
     * @return \think\response\Json
     */
    public function update(UserServer $userServer) {
        $userId = session('user.id');
        $param = $this->request->only(['username' ,'desc', 'phone', 'mail'],'post');
        $this->validate($param, 'app\manager\validate\UserValidate');
        $userServer->updateUserById($userId,$param);
        return $this->jsonReturn();
    }
    /**
     * @param UserServer $userServer
     * @return \think\response\Json
     */
    public function info(UserServer $userServer) {
        $user = session('user');
        $data['id']=$user['id'];
        $data['avatar']=$user['avatar'];
        $data['username']=$user['username'];
        $data['mail']=$user['mail'];
        $data['phone']=$user['phone'];
        $data['authorize']=$user['authorize'];
        $data['desc']=$user['desc'];
        $data['firm_name']=$user['firm']['firm_name'];
        $data['firm_id']=$user['firm']['id'];
        return $this->jsonReturn($status = REQUEST_SUCCESS, $message = '操作成功',$data );
    }

    /**
     * 退出登录
     */
    public function out()
    {
        !empty($_SESSION) && $_SESSION = [];
        [session_unset(), session_destroy()];

        LogService::write('系统管理', '用户退出系统成功');

        return $this->jsonReturn(REQUEST_SUCCESS,'退出成功');

    }

    public function php(){
        echo  phpinfo();
        exit;
    }
    public function report(){
        report('yrdy!');
        return $this->jsonReturn();
    }
}
