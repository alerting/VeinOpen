<?php

namespace app\manager\controller;

use think\App;
use think\Controller;
use think\Db;
use think\db\Query;

/**
 * 管理员控制器
 * Class Firm
 * @package app\manager\controller
 */
class Init extends Controller {

    /**
     * 初始设备列表
     * @return \think\response\Json
     */
    public function index() {
        $search = $this->request->only(['device_type_id', 'device_id'], 'get');
        $query = new Query();

        $query->where('firm_id', 0);
        foreach (['device_type_id', 'device_id'] as $key) {
            if ((isset($search[$key]) && $search[$key] !== '')) {
                $query->where($key, "$search[$key]");
            }
        }

        $result = Db::table('open_device')->alias('d')->join('open_device_type dt', 'd.device_type_id=dt.id')->where($query)
                    ->field('d.id,d.device_id,dt.device_type_name,d.theme,d.module,d.firm_id')->paginate(10);

        return $this->jsonReturn(REQUEST_SUCCESS,'操作成功', $result);
    }


    /**
     * 初始设备划拨
     * @param $id
     * @return \think\response\Json
     */

    public function allot($id) {
        //验证数据
        $firmId = $this->request->post('firm_id');

        $result = Db::table('open_device')->where('id', $id)->update(['firm_id' => $firmId]);

        //返回数据
        return $this->jsonReturn();
    }


    /**
     * 删除初始设备
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete($id) {

        Db::table('open_device')->where('id', $id)->where('firm_id', 0)->update(['delete_at'=>time()]);
        return $this->jsonReturn();
    }

}
