<?php

namespace app\manager\controller;

use app\common\event\events\LoginSuccessEvent;
use app\common\exception\AuthException;
use service\LogService;
use think\Controller;
use app\manager\model\Menu as MenuModel;
use \app\manager\model\User as UserModel;
use think\Db;


class Login extends Controller
{

    /**
     * 控制器基础方法
     */
    public function initialize()
    {
        if (session('user.id') && !in_array($this->request->action() ,['miss'])) {
            throw new AuthException('您已经登陆!',HAS_LOGINED_ERROR);
        }
    }

    /**
     * 用户登录
     * @return string
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index( UserModel $userModel)
    {
        // 输入数据效验
        $param = $this->request->only(['username' ,'password',],'post');
        $this->validate($param, 'app\manager\validate\LoginValidate');

        // 用户信息验证
        $user = $userModel->where(['username' => $param['username']])->findOrEmpty()->toArray();

        if(empty($user)){
            throw new AuthException('登录账号不存在，请重新输入!',LOGIN_ERROR);
        }
        if(empty($user['status'])){
            throw new AuthException('账号已经被禁用，请联系管理员!');
        }
        if($user['password'] !== md5($param['password'])){
            throw new AuthException('登录密码错误，请重新输入!',LOGIN_ERROR);
        }
        //todo 商家状态 需要验证?
        //触发登陆成功事件
        triggerEvent(new LoginSuccessEvent($user));
        $nodes=session('user.nodes');
        $redirectUrl= MenuModel::where(['status' => '1'])->whereIn('url',$nodes)
                           ->order('sort asc,id asc')
                           ->column('furl');
        $sessionToken=session_id();
        $redirectUrl="/manager/dashboard";
        return $this->jsonReturn(REQUEST_SUCCESS,'登陆成功',['redirectUrl'=>$redirectUrl,'sessionToken'=>$sessionToken]);
    }


    /**
     * miss路由 所有错误路由转发到这儿
     * @return \think\response\Json
     */
    public function miss() {
        return $this->jsonReturn(1, '请求地址错误');
    }

}
