<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/5
 * Time: 16:55
 * description:描述
 */

namespace app\manager\validate;

use think\Db;
use think\Validate;

class MenuValidate extends Validate {
    //todo 验证父级菜单存在
    protected $rule =   [
//        'id' => 'number',
        'pid' => 'integer',
        'title' => 'chsDash',
        'url' => 'checkUrl:thinkphp',
        'furl' => 'regex:^\/\S+',
        'is_menu'=>'in:0,1'
    ];

    protected $message  =   [
//        'id.number' => 'ID格式错误！',
        'pid.number' => '父节点ID无效！',
        'pid.require' => '父节点不能为空！',
        'title.chsDash'     => '节点名称无效!',
        'title.require'     => '节点名称不能为空!',
        'url.require' => '后端节点不能为空！',
        'furl.require' => '前端URL不能为空！',
        'furl.regex' => '前端URi格式错误！',
        'is_menu.in' => '是否菜单选项无效',
    ];

    public function sceneCreate()
    {
        return $this->only(['pid', 'title', 'url', 'furl','is_menu', ])
            ->append('pid', 'require')
            ->append('title', 'require')
            ->append('is_menu', 'require')
            ->append('furl', 'require');
    }

    // 自定义验证规则
    protected function checkUrl($url, $rule, $data = []) {

        $message=null;
        if($url){
            $result = Db::name('system_node')->where('node', $url)->findOrEmpty();
            if (!$result) {
                $message='不存在的节点!';
            }
        }
        return $message ?: true;
    }
}
