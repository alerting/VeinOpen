<?php

//$curl = curl_init();
//
//curl_setopt_array($curl, [CURLOPT_URL            => "https://oapi.dingtalk.com/robot/send?access_token=6075a0e7b2ff4bba5cced829d44c65b394978b78aaef5d28b5598127b5a19713",
//                          CURLOPT_RETURNTRANSFER => true, CURLOPT_ENCODING => "", CURLOPT_MAXREDIRS => 10,
//                          CURLOPT_TIMEOUT        => 1, CURLOPT_FOLLOWLOCATION => true,
//                          CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "POST",
//                          CURLOPT_POSTFIELDS     => "{\n    \"msgtype\": \"text\",\n    \"text\": {\n        \"content\": \"伙伴sd\"\n    },\n    \"at\": {\n        \"atMobiles\": [],\n        \"isAtAll\": false\n    }\n}",
//                          CURLOPT_HTTPHEADER     => ["Content-Type: application/json"],]);
//
//list($content, $status,$error) = [curl_exec($curl), curl_getinfo($curl),curl_error($curl) , curl_close($curl)];
//$httpCode = intval($status["http_code"]);
//if ($httpCode !== 200) {
//    echo('HTTP请求状态=' . $httpCode . '响应信息=' . (is_array($content) ? json_encode($content) :
//            $content) . ':状态详情' . json_encode($status).':错误详情'.$error);
//} else {
//    echo 'success:' . $content;
//}


abstract class food
{
    protected $foodType = 'common';
    public function getFoodType(){
        return $this->foodType;
    }
}

class egg extends food
{
    protected $foodType = 'hasShell';
}


class microwave
{
    private $using = false;
    private $forbidFoodType = ['hasShell'];

    //开门
    public function openDoor()
    {
        try {
            if ($this->using) {
                throw new \Exception('正在使用中不能开门!', 10000);
            }
            $this->openDoorHandle();
            $return = ['status' => 0, 'message' => '操作成功'];
        } catch (\Throwable $e) {
            $return = ['status' => $e->getCode() ?: 1, 'message' => $e->getMessage()];
        }
        return json_encode($return);
    }

    private function openDoorHandle()
    {
    }

    //加热
    public function heating(Food $food)
    {
        try {
            $this->checkFood($food);
            $this->heatingHandle($food);
            $return = ['status' => 0, 'message' => '操作成功'];
        } catch (\Throwable $e) {
            $return = ['status' => $e->getCode() ?: 1, 'message' => $e->getMessage()];
        }
        return json_encode($return);
    }

    private function checkFood(Food $food)
    {
        if(in_array($food->getFoodType(),$this->forbidFoodType)){
            throw new \Exception('该食物不能使用微波炉加热!',10001);
        }
    }

    private function heatingHandle(Food $food)
    {
        $this->using=true;
    }
}