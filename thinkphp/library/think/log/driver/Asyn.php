<?php

namespace think\log\driver;

use app\common\dataset\RequestLog;
use think\App;

class Asyn {

    private $config = ['messageQueue' => 'redis', 'project' => 'default',];
    private $redis;


    public function __construct(App $app, array $config = []) {
        $this->app = $app;

        if (!empty($config)) {
            $this->config = array_merge($this->config, $config);
        }
    }


    public function save(array $log, $append = false) {
        try {
            $topic = $this->config['project'];
            if ($this->app->isDebug() && $append) {
                $this->getDebugLog($log);
            }
            $request = ['ip'    => $this->app['request']->ip(), 'method' => $this->app['request']->method(),
                        'host'  => $this->app['request']->host(), 'uri' => $this->app['request']->url(),
                        'param' => $this->app['request']->param(),];
            $log['ext'] = RequestLog::getInstance()->toArray();
            $log = $request + $log;
            $log['project'] = $topic;
            $log['serverIp'] = gethostbyname(gethostname());
            $log['time'] = date('Y-m-d H:i:s', time());
            $logId=$log['logId'][0];
            $log['logId']= $logId;
            return $this->send($log, $topic);
        } catch (\Throwable $e) {

            report('写入asyn日志异常:文件' . $e->getFile() . ' 第' . $e->getLine() . '行;' . $e->getMessage().'内容:'.json_encode($log));
        }
    }

    private function send(array $content, $topic) {

       pushLogEvent($topic,$content);
       return true;
    }



    protected function getDebugLog(&$info) {

        // 获取基本信息
        $runtime = round(microtime(true) - $this->app->getBeginTime(), 10);
        $reqs = $runtime > 0 ? number_format(1 / $runtime, 2) : '∞';

        $memory_use = number_format((memory_get_usage() - $this->app->getBeginMem()) / 1024, 2);

        $info = ['runtime' => number_format($runtime, 6) . 's', 'reqs' => $reqs . 'req/s',
                 'memory'  => $memory_use . 'kb', 'file' => count(get_included_files()),] + $info;

    }


}
