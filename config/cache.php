<?php


return [
    // 驱动方式
    'type'	=>	'redis',
    'host'       => env('redis.master_hostname','127.0.0.1'),
    // redis端口
    'port'       => env('redis.master_hostport','6379'),
    // 密码
    'password'   =>  env('redis.master_auth',''),
    // 缓存保存目录
    //'path'   => CACHE_PATH,
    // 缓存前缀
    'prefix' => 'CACHE_',
    // 缓存有效期 0表示永久缓存
    'expire' => 7200,
];