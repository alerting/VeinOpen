<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2020/02/14
 * Time: 17:31
 * description:描述
 */

return [
    'messageQueue' => env('log.message_queue', 'redis'),
    'QueueServer' => [
        'host' => env('redis.master_hostname', '127.0.0.1'),
        'port' => env('redis.master_port', '6379'),
        'auth' => env('redis.master_auth', 'secret'),],
    'LogHost' => env('log.loghost', '127.0.0.1'),
    'LogPort' => env('log.logport', '9601'),
    ];