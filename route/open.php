<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/12
 * Time: 17:09
 * description:描述
 */
use think\facade\Route;


Route::group('open', function () {

    //场馆
    Route::resource('space','space')->except(['create']);
    Route::resource('device','device')->except(['create']);

    Route::post('api', 'api/update');
    Route::get('api', 'api/index');
    Route::get('member', 'member/index');

    Route::miss('manager/index/miss');

})->middleware(\app\manager\middleware\Auth::class)->prefix("open/")->pattern(['id' => '\d+']);



