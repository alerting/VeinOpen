<?php
/**
 * Created by PhpStorm.
 * User: zh
 * Date: 2019/12/12
 * Time: 17:09
 * description:描述
 */
use think\facade\Route;


Route::group('api', function () {

    //设备初始化
    Route::post('init', 'index/deviceInit');
    //对外API
    Route::post('bind', 'web/bindMember');
    Route::post('auth', 'web/auth');
    Route::post('del', 'web/deleteMember');
    //设备API
    Route::post('devicebind', 'device/bindMember');
    Route::post('deviceauth', 'device/auth');

    Route::miss('manager/index/miss');

})->prefix("api/")->pattern(['id' => '\d+']);

//miss

