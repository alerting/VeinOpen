<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: veinservice.proto

namespace Protobuf;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>protobuf.DelVeinReq</code>
 */
class DelVeinReq extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string RangeFlag = 1;</code>
     */
    private $RangeFlag = '';
    /**
     * Generated from protobuf field <code>bytes Model = 2;</code>
     */
    private $Model = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $RangeFlag
     *     @type string $Model
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Veinservice::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string RangeFlag = 1;</code>
     * @return string
     */
    public function getRangeFlag()
    {
        return $this->RangeFlag;
    }

    /**
     * Generated from protobuf field <code>string RangeFlag = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setRangeFlag($var)
    {
        GPBUtil::checkString($var, True);
        $this->RangeFlag = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bytes Model = 2;</code>
     * @return string
     */
    public function getModel()
    {
        return $this->Model;
    }

    /**
     * Generated from protobuf field <code>bytes Model = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setModel($var)
    {
        GPBUtil::checkString($var, False);
        $this->Model = $var;

        return $this;
    }

}

