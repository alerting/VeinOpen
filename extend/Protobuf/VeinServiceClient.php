<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Protobuf;

/**
 */
class VeinServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Protobuf\AddVeinReq $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateVein(\Protobuf\AddVeinReq $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/protobuf.VeinService/CreateVein',
        $argument,
        ['\Protobuf\AddVeinRes', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Protobuf\MatchVeinReq $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function MatchVein(\Protobuf\MatchVeinReq $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/protobuf.VeinService/MatchVein',
        $argument,
        ['\Protobuf\MatchVeinRes', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Protobuf\DelVeinReq $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function DeleteVein(\Protobuf\DelVeinReq $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/protobuf.VeinService/DeleteVein',
        $argument,
        ['\Protobuf\DelVeinRes', 'decode'],
        $metadata, $options);
    }

}
